//
//  UIImage+WKColorImage.m
//  DCMall
//
//  Created by 吴恺 on 15/8/19.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import "UIImage+ImageClip.h"

@implementation UIImage (AppImageCategory)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
  CGRect rect = CGRectMake(0, 0, size.width, size.height);
  UIGraphicsBeginImageContext(rect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, color.CGColor);
  CGContextFillRect(context, rect);
  UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return img;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
  return [self imageWithColor:color size:CGSizeMake(1, 1)];
}

- (UIImage *)scaleImageToScale:(float)scaleSize {
  UIGraphicsBeginImageContext(
      CGSizeMake(self.size.width * scaleSize, self.size.height * scaleSize));
  [self drawInRect:CGRectMake(0, 0, self.size.width * scaleSize,
                              self.size.height * scaleSize)];
  UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return scaledImage;
}

- (UIImage *)aspectFill:(CGSize)size offset:(CGFloat)offset {
  int wantW = size.width;
  int wantH = size.height;
  int imageW = self.size.width;
  int imageH = self.size.height;
  CGImageRef imageRef = self.CGImage;
  CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
  CGContextRef bitmap = CGBitmapContextCreate(
      NULL, wantW, wantH, 8, 4 * wantW, colorSpaceInfo,
      kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
  if (self.imageOrientation == UIImageOrientationLeft ||
      self.imageOrientation == UIImageOrientationRight) {
    wantW = size.height;
    wantH = size.width;
    imageW = self.size.height;
    imageH = self.size.width;
  }
  double ratio = MAX(wantW / (double)imageW, wantH / (double)imageH);
  imageW = ratio * imageW;
  imageH = ratio * imageH;

  int dW = abs((imageW - wantW) / 2);
  int dH = abs((imageH - wantH) / 2);

  if (dW == 0) {
    dH += offset;
  }
  if (dH == 0) {
    dW += offset;
  }

  [self imageOrientationWidth:wantW andHeight:wantH andBitmap:bitmap];
  CGContextDrawImage(bitmap, CGRectMake(-dW, -dH, imageW, imageH), imageRef);
  CGImageRef ref = CGBitmapContextCreateImage(bitmap);
  UIImage *newImage = [UIImage imageWithCGImage:ref];
  CGColorSpaceRelease(colorSpaceInfo);
  CGImageRelease(ref);
  return newImage;
}

- (void)imageOrientationWidth:(int)w
                    andHeight:(int)h
                    andBitmap:(CGContextRef)bitmap {
  if (self.imageOrientation == UIImageOrientationLeft ||
      self.imageOrientation == UIImageOrientationLeftMirrored) {
    CGContextRotateCTM(bitmap, M_PI / 2);
    CGContextTranslateCTM(bitmap, 0, -h);
  } else if (self.imageOrientation == UIImageOrientationRight ||
             self.imageOrientation == UIImageOrientationRightMirrored) {
    CGContextRotateCTM(bitmap, -M_PI / 2);
    CGContextTranslateCTM(bitmap, -w, 0);
  } else if (self.imageOrientation == UIImageOrientationUp ||
             self.imageOrientation == UIImageOrientationUpMirrored) {
    // 不作处理
  } else if (self.imageOrientation == UIImageOrientationDown ||
             self.imageOrientation == UIImageOrientationDownMirrored) {
    CGContextTranslateCTM(bitmap, w, h);
    CGContextRotateCTM(bitmap, -M_PI);
  }
}

@end
