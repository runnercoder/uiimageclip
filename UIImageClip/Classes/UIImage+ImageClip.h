//
//  UIImage+WKColorImage.h
//  DCMall
//
//  Created by 吴恺 on 15/8/19.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (AppImageCategory)
//生成size为(1,1)的纯色图片
+ (UIImage *)imageWithColor:(UIColor *)color;
//生成指定大小和颜色的图片
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
//裁剪图片,scale为裁剪比例
- (UIImage *)scaleImageToScale:(float)scaleSize;
@end
