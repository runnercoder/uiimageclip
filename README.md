# UIImageClip

[![CI Status](http://img.shields.io/travis/starrykai/UIImageClip.svg?style=flat)](https://travis-ci.org/starrykai/UIImageClip)
[![Version](https://img.shields.io/cocoapods/v/UIImageClip.svg?style=flat)](http://cocoapods.org/pods/UIImageClip)
[![License](https://img.shields.io/cocoapods/l/UIImageClip.svg?style=flat)](http://cocoapods.org/pods/UIImageClip)
[![Platform](https://img.shields.io/cocoapods/p/UIImageClip.svg?style=flat)](http://cocoapods.org/pods/UIImageClip)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UIImageClip is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UIImageClip"
```

## Author

starrykai, starryskykai@gmail.com

## License

UIImageClip is available under the MIT license. See the LICENSE file for more info.
